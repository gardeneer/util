package net.icolino.util.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import net.icolino.util.exception.BackupException;

import org.apache.commons.net.ftp.FTPClient;

public class FTPBackup implements IBackup {
	
	public static final String FTP_SERVER = "ftp.server";
	public static final String FTP_USER = "ftp.user";
	public static final String FTP_PASSWORD = "ftp.password";
	public static final String FTP_FOLDER = "ftp.folder";
	
	private FTPClient client;
	private String ftpServer;
	private String ftpUser;
	private String ftpPassword;
	
	public FTPBackup(String ftpServer, String ftpUser, String ftpPassword) {
		this.client = null;
		this.ftpServer = ftpServer;
		this.ftpUser = ftpUser;
		this.ftpPassword = ftpPassword;
	}

	private boolean openConnection(String folder) throws BackupException {
		boolean result = false;
		try {
			client = new FTPClient();
			client.connect(ftpServer);
			result = client.login(ftpUser, ftpPassword);
			client.enterLocalPassiveMode();
			client.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);
			client.changeWorkingDirectory(folder);
		} catch (IOException ioe) {
			throw new BackupException("FTPBackup. Openning connection", ioe.getMessage());
		}
		return result;
	}
	
	private boolean closeConnection() throws BackupException {
		boolean result = false;
		try {
			if (client.isConnected()) {
				client.logout();
				client.disconnect();
			}
			result = true;
		} catch (IOException ioe) {
			throw new BackupException("FTPBackup. Closing connection ", ioe.getMessage());
		}
		return result;
	}
	
	public boolean backupFile(String filename, String folder) throws BackupException
	{
		boolean backup = false;
		try {
			if (openConnection(folder)) {
				File file = new File(filename);
				InputStream inputStream = new FileInputStream(file);
				boolean upload = client.storeFile(filename, inputStream);
				if (!upload) {
					System.out.println("File upload failed");
					switch (client.getReplyCode()) {
						case 550: 
							// Permission denied
							System.out.println(client.getReplyString());
							break;
							// Pending to define
						default:
							System.out.println(client.getReplyString());
					}
				}
				inputStream.close();
				backup = true;
			}
		} catch (IOException ioe) {
			System.out.println("Error : " + ioe.getMessage());
			ioe.printStackTrace();
		} finally {
			closeConnection();
		} 
		return backup;
	}

	public boolean backupFiles(List<String> files, String folder) throws BackupException
	{
		boolean backup = false;
		try {
			if (openConnection(folder)) {
				for(String s : files) 
				{
					File file = new File(s);
					InputStream inputStream = new FileInputStream(file);
					boolean upload = client.storeFile(s, inputStream);
					if (!upload) {
						switch (client.getReplyCode()) {
							case 550: 
								// Permission denied
								throw new BackupException("FTPBackup", client.getReplyString());
						}
					}
					inputStream.close();
				}
				backup = true;
			}
		} catch (IOException ioe) {
			System.out.println("Error : " + ioe.getMessage());
			ioe.printStackTrace();
		} finally {
			closeConnection();
		} 
		return backup;
	}
}
