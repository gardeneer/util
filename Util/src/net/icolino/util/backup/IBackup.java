package net.icolino.util.backup;

import java.util.List;

import net.icolino.util.exception.BackupException;

public interface IBackup {

	/**
	 * This method backups a single file 
	 * @param filename File to backup
	 * @param folder Path where backup the file
	 * @return true if backup is done sucessfully, otherwise false
	 * @throws BackupException Indicates method and causes
	 */
	public boolean backupFile(String filename, String folder) throws BackupException;
	
	/**
	 * This method backup a list of files
	 * @param files list of files to backup
	 * @param folder Path where backup the file
	 * @return true if backup is done to all of files, otherwise false
	 * @throws BackupException Indicates method and causes
	 */
	public boolean backupFiles(List<String> files, String folder) throws BackupException;
}
