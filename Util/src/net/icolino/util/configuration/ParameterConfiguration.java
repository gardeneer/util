package net.icolino.util.configuration;

import java.io.FileInputStream;
import java.util.Properties;

public class ParameterConfiguration {

	private static Properties properties = new Properties();
	
	private static final String FILENAME = "configuration.properties";
	
	static {
		try {
			properties.load(new FileInputStream(FILENAME));
		} catch (Exception e) {
			try {
				properties.load(Class.class.getResourceAsStream(FILENAME));
			} catch (Exception e2) {
				System.out.println("Reading from outer file: " + e2.getMessage());
			}
			System.out.println("Reading from outer file: " +  e.getMessage());
		}
	}
	
	private ParameterConfiguration() {}
	
	public static String getParameter(String key) {
		return properties.getProperty(key);
	}
}
