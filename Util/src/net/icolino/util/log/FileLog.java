package net.icolino.util.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import net.icolino.util.exception.LogException;

public class FileLog implements ILog {

	private static FileHandler fileTxt;

	private static SimpleFormatter formatterTxt;
	
	public static final String LOGNAME = "LOGNAME";
	public static final String LOG_LEVEL = "log.level";

	private static FileLog instance = null;
	
	private static boolean enabled;

	private final static Logger logger = Logger
			.getLogger(FileLog.LOGNAME);
	
	private FileLog() {
		enabled = true;
	}
	
	public static FileLog getInstance() {
		if (instance == null) 
		{
			instance = new FileLog();
		}
		return instance;
	}
	
	public void setup(String logName) throws LogException {
	
	    try {
			fileTxt = new FileHandler(logName + ".log");
		} catch (SecurityException se) {
			throw new LogException("Error setup FileLog", se.getMessage());
		} catch (IOException ioe) {
			throw new LogException("Error setup FileLog", ioe.getMessage());
		}
		    
	    // Create txt Formatter
	    formatterTxt = new SimpleFormatter();
	    fileTxt.setFormatter(formatterTxt);
	    fileTxt.setLevel(Level.SEVERE);
	    logger.addHandler(fileTxt);
	}	
	
	public void setLevel(int newLevel) throws LogException {
		Level level = Level.ALL;
		try {
			switch (newLevel) {
			case 0:
				level = Level.ALL;
				break;
			case 1:
				level = Level.FINEST;
				break;
			case 2:
				level = Level.FINER;
				break;
			case 3:
				level = Level.FINE;
				break;
			case 4:
				level = Level.INFO;
				break;
			case 5:
				level = Level.WARNING;
				break;
			case 6:
				level = Level.SEVERE;
				break;
			}
		} catch (Exception e) {
			level = Level.ALL;
		}
	    fileTxt.setLevel(level);
	}
	
	public void setEnabled(boolean newEnabled) {
		enabled = newEnabled;
	}
	
	public void info(String message) {
		if (enabled) {
			logger.info(message);
		}
	}
	
	public void severe(String message) {
		if (enabled) {
			logger.severe(message);
		}
	}
	
	public void warning(String message) {
		if (enabled) {
			logger.warning(message);
		}
	}

}
