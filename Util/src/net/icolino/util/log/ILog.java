package net.icolino.util.log;

import net.icolino.util.exception.LogException;

public interface ILog {

	/**
	 * Configure Log
	 * @param logname name for log to difference
	 * @throws LogException
	 */
	public void setup(String logname) throws LogException;
	
	/**
	 * Set level filter
	 * @param level
	 * @throws LogException
	 */
	public void setLevel(int level) throws LogException;
	
	/**
	 * Activate or deactivate write to log
	 * @param enabled
	 * @throws LogException
	 */
	public void setEnabled(boolean enabled) throws LogException;
	
	/**
	 * Puts an info message in log
	 * @param message message to write
	 */
	public void info(String message);

	/**
	 * Puts an severe message in log
	 * @param message message to write
	 */
	public void severe(String message);

	/**
	 * Puts an warning message in log
	 * @param message message to write
	 */
	public void warning(String message);
}
