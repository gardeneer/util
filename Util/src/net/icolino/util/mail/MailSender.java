package net.icolino.util.mail;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import net.icolino.util.exception.MailException;

public class MailSender implements IMail {
	
	private Session session;
	
	public MailSender() {
		this.session = null;
	}

	@Override
	public void sendMail(String from, String to, String[] cc,
			String[] bcc, String subject, String body) throws MailException {
		// creates a new email message
		Message message = new MimeMessage(this.session);
		
		// sets sender email
		try {
			message.setFrom(new InternetAddress(from));
		} catch (AddressException e) {
			throw new MailException(MailException.Part.FROM, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.FROM, e.getMessage());
		}
		
		// sets to receipt
		try {
			InternetAddress toAddress = new InternetAddress(to);
			message.setRecipient(Message.RecipientType.TO, toAddress);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.TO, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.TO, e.getMessage());
		}
        
        // sets cc copies
		try {
			int size = (cc != null) ? cc.length : 0;
	        InternetAddress[] ccAddresses = new InternetAddress[size];
	        int i = 0;
	        for (String s : cc)
	        {
	        	ccAddresses[i++] = new InternetAddress(s);
	        }
	        message.setRecipients(Message.RecipientType.CC, ccAddresses);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.CC, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.CC, e.getMessage());
		}
        
        // sets bcc copies
		try {
			int size = (bcc != null) ? bcc.length : 0;
	        InternetAddress[] bccAddresses = new InternetAddress[size];
	        int i = 0;
	        for (String s : bcc)
	        {
	        	bccAddresses[i++] = new InternetAddress(s);
	        }
	        message.setRecipients(Message.RecipientType.BCC, bccAddresses);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.BCC, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.BCC, e.getMessage());
		}
        
		// sets subject
        try {
			message.setSubject(subject);
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.SUBJECT, e.getMessage());
		}
 
        // sets body
        try {
			message.setContent(body, "text/html");
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.BODY, e.getMessage());
		}
        
        // sends the e-mail
        try {
			Transport.send(message);
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.SEND, e.getMessage());
		}
	}

	@Override
	public void sendMailWithAttachment(String from, String to,
			String[] cc, String[] bcc, String subject, String body,
			List<String> attachment) throws MailException {
		// creates a new email message
		Message message = new MimeMessage(this.session);
		
		// sets sender email
		try {
			message.setFrom(new InternetAddress(from));
		} catch (AddressException e) {
			throw new MailException(MailException.Part.FROM, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.FROM, e.getMessage());
		}
		
		// sets to receipt
		try {
			InternetAddress toAddress = new InternetAddress(to);
			message.setRecipient(Message.RecipientType.TO, toAddress);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.TO, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.TO, e.getMessage());
		}
        
        // sets cc copies
		try {
			int size = (cc != null) ? cc.length : 0;
	        InternetAddress[] ccAddresses = new InternetAddress[size];
	        int i = 0;
	        for (String s : cc)
	        {
	        	ccAddresses[i++] = new InternetAddress(s);
	        }
	        message.setRecipients(Message.RecipientType.CC, ccAddresses);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.CC, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.CC, e.getMessage());
		}
        
        // sets bcc copies
		try {
			int size = (bcc != null) ? bcc.length : 0;
	        InternetAddress[] bccAddresses = new InternetAddress[size];
	        int i = 0;
	        for (String s : bcc)
	        {
	        	bccAddresses[i++] = new InternetAddress(s);
	        }
	        message.setRecipients(Message.RecipientType.BCC, bccAddresses);
		} catch (AddressException e) {
			throw new MailException(MailException.Part.BCC, e.getMessage());
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.BCC, e.getMessage());
		}
        
        try {
			message.setSubject(subject);
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.SUBJECT, e.getMessage());
		}
       // message.setSentDate(new Date());
 
        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        try {
			messageBodyPart.setContent(body, "text/html");
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.BODY, e.getMessage());
		}
 
        // creates multi-part
        Multipart multipart = new MimeMultipart();
        try {
	        multipart.addBodyPart(messageBodyPart);
	 
	        // adds attachments
	        if ((attachment != null) && (!attachment.isEmpty())) {
	            for (String filePath : attachment) {
	                MimeBodyPart attachPart = new MimeBodyPart();
	 
                    attachPart.attachFile(filePath);
	                multipart.addBodyPart(attachPart);
	            }
	        }
	 
	        // sets the multi-part as e-mail's content
	        message.setContent(multipart);
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.ATTACHMENT, e.getMessage());
		} catch (IOException e) {
			throw new MailException(MailException.Part.ATTACHMENT, e.getMessage());
		}
 
        // sends the e-mail
        try {
			Transport.send(message);
		} catch (MessagingException e) {
			throw new MailException(MailException.Part.SEND, e.getMessage());
		}
    }
		

	@Override
	public void configureMail(String host, String port, final String username,
			final String password, Boolean authentication, Boolean starttls)
			throws MailException {
		
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", authentication.toString());
		properties.put("mail.smtp.starttls.enable", starttls.toString());
		properties.put("mail.user", username);
		properties.put("mail.password", password);
		
		// creates a new session with authenticator if authentication is required
		if (authentication) {
			Authenticator authenticator = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};
			this.session = Session.getInstance(properties, authenticator);
		} else {
			this.session = Session.getInstance(properties);
		}
	}
	
	@Override
	public void configureMailWithSSL(String host, String port, final String username,
			final String password, Boolean authentication) 
				throws MailException {
		
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtps.host", host);
		properties.put("mail.smtps.port", port);
		properties.put("mail.smtps.auth", "true");
		properties.put("mail.user", username);
		properties.put("mail.password", password);
		properties.put("mail.transport.protocol", "smtps");
		
		// creates a new session with authenticator if authentication is required
		if (authentication) {
			Authenticator authenticator = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};
			this.session = Session.getInstance(properties, authenticator);
		} else {
			this.session = Session.getInstance(properties);
		}
	}

	@Override
	public void setDebugMode(boolean debugMode) {
		this.session.setDebug(debugMode);
	}

}
