package net.icolino.util.mail;

import java.util.List;

import net.icolino.util.exception.MailException;

public interface IMail {

	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_STARTTTLS_ENABLE = "mail.smtp.starttls.enable";
	public static final String MAIL_USER = "mail.user";
	public static final String MAIL_PASSWORD = "mail.password";
	public static final String MAIL_FROM = "mail.from";
	public static final String MAIL_TO = "mail.to";
	public static final String MAIL_CC = "mail.cc";
	public static final String MAIL_BCC = "mail.bcc";


	/**
	 * This method send an email with no attachment
	 * @param from Sender email
	 * @param to Destinatary email
	 * @param cc List of copies
	 * @param bcc List of hidden copies
	 * @param subject Topic of email
	 * @param body Body of email
	 * @throws MailException A subtype of MailException is thrown when an error ocurrs
	 */
	public void sendMail(String from, String to, String[] cc, String[] bcc, 
			String subject, String body)
		throws MailException;
	
	/**
	 * This method send an email with attachment
	 * @param from Sender email
	 * @param to Destinatary email
	 * @param cc List of copies
	 * @param bcc List of hidden copies
	 * @param subject Topic of email
	 * @param body Body of email
	 * @param attachment List of files to attach
	 * @throws MailException A subtype of MailException is thrown when an error ocurrs
	 */
	public void sendMailWithAttachment(String from, String to, String[] cc, 
			String[] bcc, String subject, String body, List<String> attachment)
		throws MailException;
	
	/**
	 * This method configure email smtp properties
	 * @param host SMTP host
	 * @param port SMTP port
	 * @param username SMTP username
	 * @param password SMPT password
	 * @param authentication Indicates if authentication is required
	 * @param starttls Indicates if secure connection is used
	 * @throws MailException A subtype of MailException is thrown when an error ocurrs
	 */
	public void configureMail(String host, String port, String username, String password,
			Boolean authentication, Boolean starttls)
		throws MailException;
	
	/**
	 * This method configure email smtps properties
	 * @param host SMTP host
	 * @param port SMTP port
	 * @param username SMTP username
	 * @param password SMPT password
	 * @param authentication Indicates if authentication is required
	 * @throws MailException A subtype of MailException is thrown when an error ocurrs
	 */
	public void configureMailWithSSL(String host, String port, String username, 
			String password, Boolean authentication)
		throws MailException;
	
	/**
	 * Activate or deactivate debug mode
	 * @param debugMode set debugMode mode
	 */
	public void setDebugMode(boolean debugMode);
	
}
