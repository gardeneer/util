package net.icolino.util.sql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import net.icolino.util.exception.InstanceNotFoundException;
import net.icolino.util.exception.InternalErrorException;

public final class PlainActionProcessor {

	private PlainActionProcessor() {}

	public final static Object process(DataSource dataSource,
			NonTransactionalPlainAction action)
					throws InternalErrorException, InstanceNotFoundException {
		
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			return action.execute(connection);
		} catch (SQLException e) {
			throw new InternalErrorException(e);
		} finally {
			CloseOperations.closeConnection(connection);
		}
	}
	
	public final static Object process(DataSource dataSource,
			TransactionalPlainAction action)
					throws InternalErrorException, InstanceNotFoundException {

		Connection connection = null;
		boolean rollback = false;

		try {
			connection = dataSource.getConnection();
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			connection.setAutoCommit(false);

			Object result = action.execute(connection);

			return result;
		} catch(SQLException e) {
			rollback = true;
			throw new InternalErrorException(e);
		} catch(InternalErrorException e) {
			rollback = true;
			throw e;
		} catch(RuntimeException e) {
			rollback = true;
			throw e;
		} catch(Error e) {
			rollback = true;
			throw e;
		} finally {
			try {
				/* Commit or rollback, and finally, close connection. */
				if (connection != null) {
					if (rollback) {
						connection.rollback();
					} else {
						connection.commit();
					}
					CloseOperations.closeConnection(connection);
				}
			} catch (SQLException e) {
				throw new InternalErrorException(e);
			}
		}
	}
}
