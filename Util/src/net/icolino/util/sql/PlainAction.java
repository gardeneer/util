package net.icolino.util.sql;

import java.sql.Connection;

import net.icolino.util.exception.InstanceNotFoundException;
import net.icolino.util.exception.InternalErrorException;

public interface PlainAction {

	Object execute(Connection connection) 
			throws InternalErrorException, InstanceNotFoundException;
}
