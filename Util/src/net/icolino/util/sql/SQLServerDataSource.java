package net.icolino.util.sql;

import net.icolino.util.configuration.ParameterConfiguration;

public final class SQLServerDataSource extends BasicDataSource {

	private static final String DRIVER_CLASSNAME =
			"db.sqlserver.className";
	private static final String USER = 
			"db.sqlserver.user";
	private static final String PASSWORD =
			"db.sqlserver.password";
	private static final String PORT = 
			"db.sqlserver.port";
	private static final String DATABASE =
			"db.sqlserver.database";
	private static final String SERVER =
			"db.sqlserver.server";
	
	public SQLServerDataSource() {}
	
	static {
		try {
			String driverClassName =
				ParameterConfiguration.getParameter(DRIVER_CLASSNAME);
			user = ParameterConfiguration.getParameter(USER);
			password = ParameterConfiguration.getParameter(PASSWORD);
			database = ParameterConfiguration.getParameter(DATABASE);
			port = ParameterConfiguration.getParameter(PORT);
			server = ParameterConfiguration.getParameter(SERVER);
			url = String.format("jdbc:sqlserver://%s:%s;DatabaseName=%s", server, port, database);
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			LOGGER.severe("Imposible crear la coneci�n con SQLServer");
			LOGGER.severe(e.getMessage());
		}
	}
}
