package net.icolino.util.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.icolino.util.exception.InternalErrorException;

public final class CloseOperations {

	private CloseOperations() {}
	
	public static void closeResultSet(ResultSet resultSet) 
		throws InternalErrorException {
		
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new InternalErrorException(e);
			}
		}
	}

	public static void closeStatement(Statement statement) 
		throws InternalErrorException {
		
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new InternalErrorException(e);
			}
		}
	}
	
	public static void closeConnection(Connection connection)
		throws InternalErrorException {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new InternalErrorException(e);
			}
		}
		
	}
}
