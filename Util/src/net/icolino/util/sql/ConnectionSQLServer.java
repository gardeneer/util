package net.icolino.util.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import net.icolino.util.configuration.ParameterConfiguration;
import net.icolino.util.log.FileLog;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class ConnectionSQLServer extends ConnectionSQL {

	private static final String DB_SERVER = "db.sqlserver.server";
	private static final String DB_USER = "db.sqlserver.user";
	private static final String DB_PASSWORD = "db.sqlserver.password";
	private static final String DB_PORT = "db.sqlserver.port";
	private static final String DB_NAME = "db.sqlserver.database";

	private final static Logger LOGGER = Logger
			.getLogger(FileLog.LOGNAME);

	private static final String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	static {
		try {
			Class.forName(driverClassName);
		} catch (Exception e) {
			LOGGER.severe("Error at ConnectionSQL.static: " + e.getMessage());
		}
	}

	public ConnectionSQLServer() {
	}

	public Connection getConnection() {
		Connection connection = null;
		try {
			SQLServerDataSource ds = new SQLServerDataSource();
			ds.setServerName(ParameterConfiguration.getParameter(DB_SERVER));
			ds.setPortNumber(Integer.valueOf(ParameterConfiguration.getParameter(DB_PORT)));
			ds.setDatabaseName(ParameterConfiguration.getParameter(DB_NAME));
			ds.setUser(ParameterConfiguration.getParameter(DB_USER));
			ds.setPassword(ParameterConfiguration.getParameter(DB_PASSWORD));
			connection = ds.getConnection();
		} catch (Exception e) {
			LOGGER.severe("Error at ConnectionSQL.getConnection(): "
					+ e.getMessage());
		}
		return connection;
	}

	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				LOGGER.severe("Error at ConnectionSQL.closeConnection(): "
						+ e.getMessage());
			}
		}
	}

	public static void closeSentence(Statement sentencia) {
		if (sentencia != null) {
			try {
				sentencia.close();
			} catch (SQLException e) {
				LOGGER.severe("Error at ConnectionSQL.closeSentence(): "
						+ e.getMessage());
			}
		}
	}
}
