package net.icolino.util.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;




import net.icolino.util.configuration.ParameterConfiguration;
import net.icolino.util.log.FileLog;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnectionMySQL extends ConnectionSQL {

	public static final String DB_SERVER = "db.mysql.server";
	public static final String DB_USER = "db.mysql.user";
	public static final String DB_PASSWORD = "db.mysql.password";
	public static final String DB_PORT = "db.mysql.port";
	public static final String DB_NAME = "db.mysql.database";
	
	private final static Logger LOGGER = Logger
			.getLogger(FileLog.LOGNAME);

	private static final String driverClassName = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(driverClassName);
		} catch (Exception e) {
			LOGGER.severe("Error at ConnectionSQL.static: " + e.getMessage());
		}
	}

	public ConnectionMySQL() {
	}

	public Connection getConnection() {
		Connection connection = null;
		try {
			MysqlDataSource ds = new MysqlDataSource();
			ds.setServerName(ParameterConfiguration.getParameter(DB_SERVER));
			ds.setPortNumber(Integer.valueOf(ParameterConfiguration.getParameter(DB_PORT)));
			ds.setDatabaseName(ParameterConfiguration.getParameter(DB_NAME));
			ds.setUser(ParameterConfiguration.getParameter(DB_USER));
			ds.setPassword(ParameterConfiguration.getParameter(DB_PASSWORD));
			connection = ds.getConnection();
		} catch (Exception e) {
			LOGGER.severe("Error en ConnectionSQL.obtenerConexion(): "
					+ e.getMessage());
		}
		return connection;
	}

	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				LOGGER.severe("Error at ConnectionSQL.closeConnection(): "
						+ e.getMessage());
			}
		}
	}

	public static void closeSentence(Statement sentencia) {
		if (sentencia != null) {
			try {
				sentencia.close();
			} catch (SQLException e) {
				LOGGER.severe("Error at ConnectionSQL.closeSentence(): "
						+ e.getMessage());
			}
		}
	}
}
