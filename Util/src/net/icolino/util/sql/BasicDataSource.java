package net.icolino.util.sql;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import net.icolino.util.log.FileLog;
import net.icolino.util.log.ILog;

public class BasicDataSource implements DataSource {

	protected final static ILog LOGGER = FileLog.getInstance();
	
	protected static String url;
	
	protected static String user;
	
	protected static String password;
	
	protected static String port;
	
	protected static String database;
	
	protected static String server;

	
	@Override
	public PrintWriter getLogWriter() throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void setLogWriter(PrintWriter arg0) throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void setLoginTimeout(int arg0) throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}

	@Override
	public Connection getConnection(String arg0, String arg1)
			throws SQLException {
		throw new UnsupportedOperationException("Not implemented");
	}

}
