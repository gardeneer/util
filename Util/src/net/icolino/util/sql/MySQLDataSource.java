package net.icolino.util.sql;

import net.icolino.util.configuration.ParameterConfiguration;

public final class MySQLDataSource extends BasicDataSource {

	private static final String DRIVER_CLASSNAME =
			"db.mysql.className";
	private static final String USER = 
			"db.mysql.user";
	private static final String PASSWORD =
			"db.mysql.password";
	private static final String PORT = 
			"db.mysql.port";
	private static final String DATABASE =
			"db.mysql.database";
	private static final String SERVER =
			"db.mysql.server";
	
	public MySQLDataSource() {}
	
	static {
		try {
			String driverClassName =
				ParameterConfiguration.getParameter(DRIVER_CLASSNAME);
			user = ParameterConfiguration.getParameter(USER);
			password = ParameterConfiguration.getParameter(PASSWORD);
			database = ParameterConfiguration.getParameter(DATABASE);
			port = ParameterConfiguration.getParameter(PORT);
			server = ParameterConfiguration.getParameter(SERVER);
			url = String.format("jdbc:mysql://%s:%s/%s", server, port, database);
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			LOGGER.severe("Imposible crear la coneci�n con MySQL");
			LOGGER.severe(e.getMessage());
		}
	}
}
