package net.icolino.util.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import net.icolino.util.log.FileLog;

public abstract class ConnectionSQL {

	private final static Logger LOGGER = Logger
			.getLogger(FileLog.LOGNAME);

	public abstract Connection getConnection();

	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				LOGGER.severe("Error at ConnectionSQL.closeConnection(): "
						+ e.getMessage());
			}
		}
	}

	public static void closeSentence(Statement sentence) {
		if (sentence != null) {
			try {
				sentence.close();
			} catch (SQLException e) {
				LOGGER.severe("Error at ConnectionSQL.closeSentence(): "
						+ e.getMessage());
			}
		}
	}
}
