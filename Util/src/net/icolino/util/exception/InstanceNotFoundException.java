package net.icolino.util.exception;

@SuppressWarnings("serial")
public class InstanceNotFoundException extends Exception {

	private String id;
	private String className;
	
	public InstanceNotFoundException(String id, String className) {
		this.id = id;
		this.className = className;
	}
	
	public String getMessage() {
		return "Instance of " + this.className + " associated with id " + this.id + " not found";
	}
}
