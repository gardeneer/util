package net.icolino.util.exception;

@SuppressWarnings("serial")
public class ParameterConfigurationException extends Exception {

	public ParameterConfigurationException(String method, String exception) {
		super(exception);
	}
}
