package net.icolino.util.exception;

@SuppressWarnings("serial")
public class MailException extends Exception {

	public enum Part {ATTACHMENT, BCC, BODY, CC, FROM, SEND, SUBJECT, TO, UNDEFINED};
	
	public MailException(Part part, String exception) {
		super(exception);
	}
}
