package net.icolino.util.exception;

@SuppressWarnings("serial")
public class LogException extends Exception {

	public LogException(String method, String exception) {
		super(exception);
	}
}
