package net.icolino.util.exception;

@SuppressWarnings("serial")
public class InternalErrorException extends Exception {

		private Exception exception;
	
	public InternalErrorException(Exception exception) {
		this.exception = exception;
	}
	
	public String getMessage() {
		return this.exception.getMessage();
	}
}
