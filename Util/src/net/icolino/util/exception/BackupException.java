package net.icolino.util.exception;

@SuppressWarnings("serial")
public class BackupException extends Exception {

	public BackupException(String method, String exception) {
		super(exception);
	}
}
