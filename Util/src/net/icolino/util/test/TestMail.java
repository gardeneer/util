package net.icolino.util.test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import net.icolino.util.exception.MailException;
import net.icolino.util.mail.IMail;
import net.icolino.util.mail.MailSender;

public class TestMail {

	public static void main (String args[])
	{
		IMail mail = new MailSender();
		String from = "from@example.com";
		String to = "to@example.com";
		List<String> cc = new ArrayList<String>();
		List<String> bcc = new ArrayList<String>();
		String subject = "Proba de adxunto";
		String body = "<b>Veamos el cuerpo</b>";
		List<String> attachment = new ArrayList<String>();
		
		PrintWriter writer;
		try {
			String name = "20141218 Prueba de concepto.txt";
			writer = new PrintWriter(name, "UTF-8");
			writer.println("The first line");
			writer.println("The second line");
			writer.close();
			attachment.add(name);
			name = "Nueva Prueba.txt";
			writer = new PrintWriter(name, "UTF-8");
			writer.println("The first lasdfasdfasd");
			writer.println("The second lasdfasdfasdf");
			writer.close();
			attachment.add(name);
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			mail.configureMail("smtp.example.com", "587", "from@example.com", "password", true, false);
			mail.setDebugMode(true);
			mail.sendMailWithAttachment(from, to, cc, bcc, subject, body, attachment);
		} catch (MailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
