package net.icolino.util.test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import net.icolino.util.backup.FTPBackup;
import net.icolino.util.backup.IBackup;
import net.icolino.util.exception.BackupException;

public class TestBackup {
	
	public static void main (String args[])
	{
		IBackup backup = new FTPBackup("ftp.example.com", "user", "password");
		String name = "20141218 Prueba de concepto.txt";
		PrintWriter writer;
		try {
			writer = new PrintWriter(name, "UTF-8");
			writer.println("The first line");
			writer.println("The second line");
			writer.close();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}
		try {
			backup.backupFile(name, "");
		} catch (BackupException be) {
			be.printStackTrace();
		}
	}
}
